"""Test cki/cki_tools/webhook_sentinel module."""
import unittest
from unittest import mock

import responses

from cki.cki_tools import webhook_sentinel

MSG_STALE = {
    'project_id': 1, 'build_id': 2, 'build_status': 'running', 'build_duration': 10.0,
    'build_stage': 'test',
}

MSG_OK = {
    'project_id': 1, 'build_id': 3, 'build_status': 'running', 'build_duration': 0.10,
    'build_stage': 'test',
}


class TestInit(unittest.TestCase):
    """Test __init__ functions."""

    @responses.activate
    @mock.patch('cki.cki_tools.webhook_sentinel.METRIC_STALE_WEBHOOK_COUNT', mock.Mock())
    def test_update_stale_webhook_different(self):
        """Test update_stale_webhook function. API and webhook differ."""
        responses.add(responses.GET, 'https://gitlab.com/api/v4/projects/1/jobs/2',
                      json={'status': 'success', 'duration': 10.0,
                            'finished_at': '2022-06-28 10:53:58 UTC'})
        msg = 'API data for job_id 2: status: "success", duration 10.0 - statuses are different'

        with self.assertLogs(webhook_sentinel.LOGGER, level='WARNING') as log:
            new_body = webhook_sentinel.update_stale_webhook(MSG_STALE)
            self.assertIn(msg, log.output[0])

        self.assertEqual(new_body['build_status'], 'success')
        self.assertEqual(new_body['build_duration'], 10.0)
        self.assertEqual(new_body['build_finished_at'], '2022-06-28 10:53:58 UTC')

    @responses.activate
    def test_update_stale_webhook_equal(self):
        """Test update_stale_webhook function. API and webhook don't differ."""
        responses.add(responses.GET, 'https://gitlab.com/api/v4/projects/1/jobs/2',
                      json={'status': 'running', 'duration': 10.0})
        msg = 'API data for job_id 2: status: "running", duration 10.0 - statuses are equal'

        with self.assertLogs(webhook_sentinel.LOGGER, level='WARNING') as log:
            new_body = webhook_sentinel.update_stale_webhook(MSG_STALE)
            self.assertIn(msg, log.output[0])

        self.assertIsNone(new_body, None)

    @mock.patch('cki.cki_tools.webhook_sentinel.METRIC_JOB_DURATION')
    @mock.patch('cki.cki_tools.webhook_sentinel.update_stale_webhook')
    def test_callback_webhook_ok(self, mock_update, mock_metric):
        """Test callback. Message is ok."""
        webhook_sentinel.callback(MSG_OK, None, None)

        self.assertFalse(mock_update.called)
        mock_metric.assert_has_calls([
            mock.call.labels('running', 'test'),
            mock.call.labels().observe(0.1),
        ])

    @mock.patch('cki.cki_tools.webhook_sentinel.QUEUE.send_message')
    @mock.patch('cki.cki_tools.webhook_sentinel.METRIC_JOB_DURATION')
    @mock.patch('cki.cki_tools.webhook_sentinel.update_stale_webhook')
    @mock.patch('cki.cki_tools.webhook_sentinel.RABBITMQ_EXCHANGE_DST', 'exchange_dst')
    def test_callback_webhook_nok(self, mock_update, mock_metric_job, mock_send_message):
        """Test callback. Message is not ok."""
        routing_key = 'gitlab.com.foo'
        headers = {'foo': 'bar'}
        new_message = {'new': 'message'}
        mock_update.return_value = new_message

        webhook_sentinel.callback(MSG_STALE, routing_key=routing_key, headers=headers)

        self.assertTrue(mock_update.called)
        mock_metric_job.assert_has_calls([
            mock.call.labels('running', 'test'),
            mock.call.labels().observe(10),
        ])
        mock_send_message.assert_called_with(
            new_message, routing_key,
            exchange='exchange_dst', headers=headers
        )
