"""Test traiger.py."""
import unittest
from unittest import mock

from cki.triager import triager


class TestDWObject(unittest.TestCase):
    """Test DWObject."""

    def test_attrs(self):
        """Test getting attrs."""
        data = {'a': 1, 'b': 'two'}
        obj = triager.DWObject('foo', data)

        self.assertEqual(1, obj.a)
        self.assertEqual('two', obj.b)
        self.assertEqual(None, obj.c)


class TestTriager(unittest.TestCase):
    """Test Triager."""

    def test_dry_run(self):
        """Test dry_run flag."""
        self.assertTrue(triager.Triager(dry_run=True).dry_run)
        self.assertFalse(triager.Triager().dry_run)

    def test_check_checkout(self):
        """Test check_checkout."""
        obj = triager.Triager()
        self.assertEqual([], obj.check_checkout(triager.DWObject('checkout', {'id': 1})))

    @mock.patch('cki.triager.triager.CheckoutFailureChecker.check',
                mock.Mock(return_value=['foo', 'bar']))
    def test_check_checkout_failure(self):
        """
        Test check_checkout.

        CheckoutFailureChecker returns a failure.
        """
        obj = triager.Triager()
        checkout = triager.DWObject('checkout', {'id': 1, 'valid': False})
        self.assertEqual(
            ['foo', 'bar'],
            obj.check_checkout(checkout)
        )

    @mock.patch('cki.triager.triager.CheckoutFailureChecker.check',
                mock.Mock(return_value=['foo', 'bar']))
    def test_check_checkout_failure_but_valid(self):
        """
        Test check_checkout.

        CheckoutFailureChecker returns a failure but the checkout is valid.
        """
        obj = triager.Triager()
        checkout = triager.DWObject('checkout', {'id': 1, 'valid': True})
        self.assertEqual([], obj.check_checkout(checkout))

    @mock.patch('cki.triager.triager.BuildFailureChecker.check',
                mock.Mock(return_value=['foo', 'bar']))
    def test_check_build(self):
        """
        Test check_build.

        BuildFailureChecker returns a failure.
        """
        obj = triager.Triager()
        build = triager.DWObject('build', {'id': 1, 'valid': False})
        self.assertEqual(
            ['foo', 'bar'],
            obj.check_build(build)
        )

    def test_check_build_but_valid(self):
        """Test check_build."""
        obj = triager.Triager()
        self.assertEqual([], obj.check_build(triager.DWObject('build', {'id': 1, 'valid': True})))

    @mock.patch('cki.triager.triager.TestFailureChecker.check')
    def test_check_test_ignored(self, mock_check):
        """Test check_test ignores tests according to the status."""
        cases = [
            # status, check called
            ('DONE', False),
            ('ERROR', True),
            ('FAIL', True),
            ('PASS', False),
            ('SKIP', False),
        ]

        obj = triager.Triager()

        for status, called in cases:
            mock_check.reset_mock()
            test_pass = triager.DWObject(
                'test', {'id': 1, 'status': status, 'misc': {'iid': 1}}
            )
            obj.check_test(test_pass)
            self.assertEqual(called, mock_check.called, status)

    @mock.patch('cki.triager.triager.TestFailureChecker.check',
                mock.Mock(return_value=['foo', 'bar']))
    def test_check_test(self):
        """Test check_test."""
        obj = triager.Triager()
        test = triager.DWObject('test', {'id': 1, 'status': 'FAIL'})
        self.assertEqual(
            ['foo', 'bar'],
            obj.check_test(test)
        )

    @mock.patch('cki.triager.triager.FailureChecker.check')
    def testresult_check_testresult_ignored(self, mock_check):
        """Test check_testresult ignores testresults according to the status."""
        cases = [
            # status, check called
            ('DONE', False),
            ('ERROR', True),
            ('FAIL', True),
            ('PASS', False),
            ('SKIP', False),
        ]

        obj = triager.Triager()

        for status, called in cases:
            mock_check.reset_mock()
            testresult_pass = triager.DWObject(
                'testresult', {'id': 1, 'status': status, 'misc': {'iid': 1}}
            )
            obj.check_testresult(testresult_pass)
            self.assertEqual(called, mock_check.called, status)

    @mock.patch('cki.triager.triager.FailureChecker.check',
                mock.Mock(return_value=['foo', 'bar']))
    def testresult_check_testresult(self):
        """Test check_testresult."""
        obj = triager.Triager()
        testresult = triager.DWObject('testresult', {'id': 1, 'status': 'FAIL'})
        self.assertEqual(
            ['foo', 'bar'],
            obj.check_testresult(testresult)
        )

    @staticmethod
    def test_report_issue():
        """Test report issue."""
        issues = mock.Mock()
        issues.return_value = []
        obj = mock.Mock()
        obj.issues.list = issues
        issues = [
            {'id': 1},
            {'id': 2},
        ]

        triager.Triager().report_issues(obj, issues)
        obj.issues.create.assert_has_calls(
            [
                mock.call(issue_id=1),
                mock.call(issue_id=2),
            ]
        )

    def test_report_issue_dry(self):
        """Test report issue. Dry run."""
        issues = mock.Mock()
        issues.return_value = [mock.Mock(id=1)]
        obj = mock.Mock()
        obj.issues.list = issues
        issues = [{'id': 1}, {'id': 2}]

        triager.Triager(dry_run=True).report_issues(obj, issues)
        self.assertFalse(obj.issues.create.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_test')
    def test_check_check_test_dry_run(self, checker, action_triaged):
        """Test check."""
        triager_obj = triager.Triager(dry_run=True)
        triager_obj.report_issues = mock.Mock()
        obj = triager.DWObject('test', {'id': 1, 'misc': {'iid': 1}})
        triager_obj.check('test', obj)
        self.assertTrue(checker.called)
        self.assertFalse(action_triaged.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_test')
    def test_check_check_test(self, checker, action_triaged):
        """Test check."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = triager.DWObject('test', {'id': 1, 'status': 'FAIL', 'misc': {'iid': 1}})
        triager_obj.check('test', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(action_triaged.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_checkout')
    def test_check_check_checkout(self, checker, action_triaged):
        """Test check."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = triager.DWObject('checkout', {'id': 1, 'valid': False, 'misc': {'iid': 1}})
        triager_obj.check('checkout', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(action_triaged.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_build')
    def test_check_check_build(self, checker, action_triaged):
        """Test check."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = triager.DWObject('build', {'id': 1, 'valid': False, 'misc': {'iid': 1}})
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(action_triaged.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_testresult')
    def test_check_check_testresult(self, checker, action_triaged):
        """Test check."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = triager.DWObject('testresult', {'id': 1, 'status': 'FAIL', 'misc': {'iid': 1}})
        triager_obj.check('testresult', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(action_triaged.called)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_build')
    def test_check_with_dict(self, checker, action_triaged):
        """Test check. With dict it creates a DWObject."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = {'id': 1, 'valid': False, 'misc': {'iid': 1}}
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(action_triaged.called)

    @mock.patch('cki.triager.triager.dw_client')
    @mock.patch('cki.triager.triager.Triager.check_build')
    def test_check_with_id(self, checker, dw_client):
        """Test check. With id gets the obj from DW api."""
        triager_obj = triager.Triager()
        obj = '1'
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(dw_client.kcidb.build.get)

    @mock.patch('cki.triager.triager.DWObject.action_triaged')
    @mock.patch('cki.triager.triager.Triager.check_build')
    def test_check_unfinished(self, checker, action_triaged):
        """Test check unfinished objects."""
        triager_obj = triager.Triager()
        triager_obj.report_issues = mock.Mock()
        obj = {'id': 1, 'misc': {'iid': 1}}
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertFalse(action_triaged.called)
