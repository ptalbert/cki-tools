"""KCIDB adjust_dumpfiles tests."""
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock

from cki.kcidb import adjust_dumpfiles


class TestAdjustDumpfiles(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki.kcidb.adjust_dumpfiles."""

    def test_merge_upt_dumpfiles(self):
        """Test merging UPT dumpfiles works."""
        base_kcidb_all = {
            'version': {'major': 4, 'minor': 0}
        }

        main_kcidb_all = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1'},
                {'id': 'redhat:2', 'origin': 'redhat', 'build_id': 'redhat:1'},
                {'id': 'redhat:3', 'origin': 'redhat', 'build_id': 'redhat:1'},
                {'id': 'redhat:4', 'origin': 'redhat', 'build_id': 'redhat:1'},
            ]
        }

        partial_1 = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS'},
            ]
        }

        partial_2 = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:2', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS'},
            ]
        }

        partial_3 = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:3', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS'},
                {'id': 'redhat:4', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS'},
            ]
        }

        final = {
            **base_kcidb_all,
            'tests': [
                *partial_1['tests'],
                *partial_2['tests'],
                *partial_3['tests'],
            ]
        }

        with tempfile.TemporaryDirectory() as tmpdir:
            mocked_env = {
                'UPT_RESULTS': f'{tmpdir}/upt/run.done',
                'KCIDB_DUMPFILE_NAME': f'{tmpdir}/kcidb_all.json'
            }

            with mock.patch.dict(os.environ, mocked_env):
                files = [
                    (os.environ['KCIDB_DUMPFILE_NAME'], main_kcidb_all),
                    (f'{os.environ["UPT_RESULTS"]}_1/results_1/kcidb_all.json', partial_1),
                    (f'{os.environ["UPT_RESULTS"]}_2/results_2/kcidb_all.json', partial_2),
                    (f'{os.environ["UPT_RESULTS"]}_3/results_3/kcidb_all.json', partial_3),
                ]

                for path, content in files:
                    path = pathlib.Path(path)
                    path.parent.mkdir(parents=True, exist_ok=True)
                    path.write_text(json.dumps(content))

                adjust_dumpfiles.main()

                self.assertEqual(
                    final,
                    json.loads(pathlib.Path(os.environ['KCIDB_DUMPFILE_NAME']).read_text())
                )

    def test_reruns(self):
        """Test merging UPT dumpfiles with reruns overwrites the previous entries."""
        base_kcidb_all = {
            'version': {'major': 4, 'minor': 0}
        }

        main_kcidb_all = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1'},
                {'id': 'redhat:2', 'origin': 'redhat', 'build_id': 'redhat:1'},
            ]
        }

        partial_1 = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS',
                 'misc': {'rerun_index': 1}},
            ]
        }

        partial_2 = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:2', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'ERROR',
                 'misc': {'rerun_index': 1}},
            ]
        }

        partial_2_rerun = {
            **base_kcidb_all,
            'tests': [
                {'id': 'redhat:2', 'origin': 'redhat', 'build_id': 'redhat:1', 'status': 'PASS',
                 'misc': {'rerun_index': 2}},
            ]
        }

        final = {
            **base_kcidb_all,
            'tests': [
                *partial_1['tests'],
                *partial_2_rerun['tests'],
            ]
        }

        with tempfile.TemporaryDirectory() as tmpdir:
            mocked_env = {
                'UPT_RESULTS': f'{tmpdir}/upt/run.done',
                'KCIDB_DUMPFILE_NAME': f'{tmpdir}/kcidb_all.json'
            }

            with mock.patch.dict(os.environ, mocked_env):
                files = [
                    (os.environ['KCIDB_DUMPFILE_NAME'], main_kcidb_all),
                    (f'{os.environ["UPT_RESULTS"]}_1/results_1/kcidb_test_1.json',
                     partial_1),
                    (f'{os.environ["UPT_RESULTS"]}_2/results_1/kcidb_test_2.json',
                     partial_2),
                    (f'{os.environ["UPT_RESULTS"]}_3/results_1/kcidb_test_2_1.json',
                     partial_2_rerun),
                ]

                for path, content in files:
                    path = pathlib.Path(path)
                    path.parent.mkdir(parents=True, exist_ok=True)
                    path.write_text(json.dumps(content))

                adjust_dumpfiles.main()

                self.assertEqual(
                    final,
                    json.loads(pathlib.Path(os.environ['KCIDB_DUMPFILE_NAME']).read_text())
                )
