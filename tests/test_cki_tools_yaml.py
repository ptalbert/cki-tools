"""YAML CLI tool tests."""
import contextlib
import io
import sys
import typing
import unittest

import yaml

from cki.cki_tools import yaml as cki_yaml


class TestYaml(unittest.TestCase):
    """Test YAML CLI tool."""

    @staticmethod
    @contextlib.contextmanager
    def redirect(what: str, new: typing.IO[str]) -> typing.Iterator[typing.IO[str]]:
        """Redirect a stream."""
        old = getattr(sys, what)
        setattr(sys, what, new)
        try:
            yield new
        finally:
            setattr(sys, what, old)

    def test_yaml_set_value(self):
        """Ensure set-value works."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: [qux, {bar: baz}]')):
            cki_yaml.main(['set-value', 'foo.1.bar', 'zig'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': ['qux', {'bar': 'zig'}]})

    def test_yaml_set_value_last_array(self):
        """Ensure set-value works with array elements."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: [bar, baz]')):
            cki_yaml.main(['set-value', 'foo.1', 'zig'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': ['bar', 'zig']})

    def test_yaml_set_value_dot(self):
        """Ensure set-value works with keys with dots."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo.qux: [bar, baz]')):
            cki_yaml.main(['set-value', r'foo\.qux.1', 'zig'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo.qux': ['bar', 'zig']})

    def test_yaml_del_list(self):
        """Ensure del works with lists."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: [bar, baz]')):
            cki_yaml.main(['del', 'foo.0'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': ['baz']})

    def test_yaml_del_dict(self):
        """Ensure del works."""
        with self.redirect('stdout', io.StringIO()) as stdout, \
                self.redirect('stdin', io.StringIO('foo: {bar: baz, qux: zig}')):
            cki_yaml.main(['del', 'foo.bar'])
        self.assertEqual(yaml.safe_load(stdout.getvalue()),
                         {'foo': {'qux': 'zig'}})
