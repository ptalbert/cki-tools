"""Tests refresh_dogtag_certificates."""

import os
import pathlib
import tempfile
import unittest
from unittest import mock

from OpenSSL import crypto
import responses

from cki.cki_tools import refresh_dogtag_certificates


def _example_key() -> str:
    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 512)
    return crypto.dump_privatekey(crypto.FILETYPE_PEM, key).decode('ascii')


class TestRefreshDogtagCertificates(unittest.TestCase):
    """Tests refresh_dogtag_certificates."""

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'access-key',
        'AWS_SECRET_ACCESS_KEY': 'secret-key',
    })
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.download',
                mock.Mock(return_value=b'content'))
    def test_download_only(self) -> None:
        """Test --download-only."""
        with tempfile.NamedTemporaryFile() as temp:
            refresh_dogtag_certificates.main(['--download-only', temp.name,
                                              '--s3-url', 'https://endpoint/bucket/file.yml'])
            self.assertEqual(pathlib.Path(temp.name).read_bytes(), b'content')

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'access-key',
        'AWS_SECRET_ACCESS_KEY': 'secret-key',
        'DOGTAG_URL': 'https://dog.tag:8443',
        'TEST_PRIVATE_KEY': _example_key(),
        'TEST_USER': 'user',
        'TEST_PASSWORD': 'user',
    })
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.download',
                mock.Mock(return_value=b'{}\n'))
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.upload')
    @responses.activate
    def test_up_and_down(self, mock_upload) -> None:
        """Test download and upload."""
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [], 'Link': None})
        refresh_dogtag_certificates.main(['--s3-url', 'https://endpoint/bucket/file.yml'])
        mock_upload.assert_called_with(b'{}\n')

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'access-key',
        'AWS_SECRET_ACCESS_KEY': 'secret-key',
        'DOGTAG_URL': 'https://dog.tag:8443',
        'TEST_PRIVATE_KEY': _example_key(),
        'TEST_USER': 'user',
        'TEST_PASSWORD': 'user',
    })
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.download',
                mock.Mock(return_value=b'{}\n'))
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.upload')
    @responses.activate
    def test_up_and_down_default(self, mock_upload) -> None:
        """Test download default."""
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [], 'Link': None})
        refresh_dogtag_certificates.main(['--s3-url', 'https://endpoint/bucket/file.yml'])
        mock_upload.assert_called_with(b'{}\n')

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'access-key',
        'AWS_SECRET_ACCESS_KEY': 'secret-key',
        'DOGTAG_URL': 'https://dog.tag:8443',
        'TEST_PRIVATE_KEY': _example_key(),
        'TEST_USER': 'user',
        'TEST_PASSWORD': 'user',
    })
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.download',
                mock.Mock(return_value=b'[invalid'))
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.upload')
    @responses.activate
    def test_up_and_down_invalid(self, mock_upload) -> None:
        """Test downloading invalid data."""
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [], 'Link': None})
        refresh_dogtag_certificates.main(['--s3-url', 'https://endpoint/bucket/file.yml'])
        mock_upload.assert_called_with(b'{}\n')

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'access-key',
        'AWS_SECRET_ACCESS_KEY': 'secret-key',
        'DOGTAG_URL': 'https://dog.tag:8443',
        'TEST_PRIVATE_KEY': _example_key(),
        'TEST_USER': 'user',
        'TEST_PASSWORD': 'user',
    })
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.download',
                mock.Mock(return_value=b'{}'))
    @mock.patch('cki.cki_tools.refresh_dogtag_certificates.S3File.upload')
    @responses.activate
    def test_from_server(self, mock_upload) -> None:
        """Test getting the newest cert from the server."""
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [{
                          'id': '0x1',
                          'NotValidBefore': 1629326630000,
                          'NotValidAfter': 2260478630000,
                      }, {
                          'id': '0x2',
                          'NotValidBefore': 1629326630001,
                          'NotValidAfter': 2260478630001,
                      }], 'Link': None})
        responses.add(responses.GET, 'https://dog.tag:8443/ca/rest/certs/0x2',
                      json={
                          'Encoded': 'encoded',
                      })
        refresh_dogtag_certificates.main(['--s3-url', 'https://endpoint/bucket/file.yml'])
        mock_upload.assert_called_with(b'TEST_CERTIFICATE: encoded\n')

    @mock.patch.dict(os.environ, {
        'DOGTAG_URL': 'https://dog.tag:8443',
        'TEST_PRIVATE_KEY': _example_key(),
        'TEST_USER': 'user',
        'TEST_PASSWORD': 'user',
    })
    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_refresh(self) -> None:
        """Test refreshing a cert on the server."""
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [{
                          'id': '0x1',
                          'NotValidBefore': 1629326630000,
                          'NotValidAfter': 1629326630001,
                      }], 'Link': None})
        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certs/search?maxResults=100',
                      json={'entries': [{
                          'id': '0x2',
                          'NotValidBefore': 1629326630001,
                          'NotValidAfter': 1629326630002,
                      }], 'Link': None})
        responses.add(responses.GET, 'https://dog.tag:8443/ca/rest/certs/0x1',
                      json={
                          'Encoded': 'encoded',
                      })
        responses.add(responses.GET, 'https://dog.tag:8443/ca/rest/certs/0x2',
                      json={
                          'Encoded': 'encoded',
                      })
        responses.add(responses.GET,
                      'https://dog.tag:8443/ca/rest/certrequests/profiles/caDirAppUserCert',
                      json={
                          'Attributes': {'Attribute': []},
                          'Input': [{
                              'Attribute': [
                                  {'name': 'cert_request_type', 'Value': ''},
                                  {'name': 'cert_request', 'Value': ''}
                              ],
                              'ConfigAttribute': []
                          }],
                          'Output': [],
                      })

        responses.add(responses.POST, 'https://dog.tag:8443/ca/rest/certrequests',
                      json={
                          'entries': [],
                          'Link': None})

        refresh_dogtag_certificates.main(['--refresh-before', '1d'])
        self.assertTrue(any(c.request.url == 'https://dog.tag:8443/ca/rest/certrequests'
                            for c in responses.calls))
