# CKI Tools

All the command line tools the CKI team uses.

## Tests

To run the tests, just type `tox`. That should take care of installing
all dependencies needed for it. However, you have to make sure you
install the package `krb5-devel` (with `dnf`, `yum`, or similar).

You can also run tests in a podman container via

```shell
podman run --rm -it --volume .:/code --workdir /code quay.io/cki/python:production tox
```
## Optional dependencies

To use the `brew_trigger` module, use the `brew` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[brew]
```

To use the `kcidb` module, use the `kcidb` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[kcidb]
```

## KCIDB Image.

An extra image is built to run the KCIDB module.
This image is used on OpenShift pods, and contains `kcidb` extra dependencies.

`quay.io/cki/kcidb:production`

## Detailed documentation of Python tools

| Tool                                            | Detailed documentation                                                          |
|-------------------------------------------------|---------------------------------------------------------------------------------|
| `cki.beaker_tools.broken_machines`              | [documentation](documentation/cki.beaker_tools.broken_machines.md)              |
| `cki.cki_tools.amqp_bridge`                     | [documentation](documentation/cki.cki_tools.amqp_bridge.md)                     |
| `cki.cki_tools.brew_trigger`                    | [documentation](documentation/cki.cki_tools.brew_trigger.md)                    |
| `cki.cki_tools.datawarehouse.issue_maintenance` | [documentation](documentation/cki.cki_tools.datawarehouse.issue_maintenance.md) |
| `cki.cki_tools.get_kernel_headers`              |                                                                                 |
| `cki.cki_tools.gitlab_ci_bot`                   | [documentation](documentation/cki.cki_tools.gitlab_ci_bot.md)                   |
| `cki.cki_tools.gitlab_sso_login`                | [documentation](documentation/cki.cki_tools.gitlab_sso_login.md)                |
| `cki.cki_tools.install_dependencies`            | [documentation](documentation/cki.cki_tools.install_dependencies.md)            |
| `cki.cki_tools.k8s_event_listener`              | [documentation](documentation/cki.cki_tools.k8s_event_listener.md)              |
| `cki.cki_tools.krb_ticket_refresher`            |                                                                                 |
| `cki.cki_tools.orphan_hunter`                   | [documentation](documentation/cki.cki_tools.orphan_hunter.md)                   |
| `cki.cki_tools.orphan_hunter_ec2`               | [documentation](documentation/cki.cki_tools.orphan_hunter_ec2.md)               |
| `cki.cki_tools.retrigger`                       | [documentation](documentation/cki.cki_tools.retrigger.md)                       |
| `cki.cki_tools.refresh_dogtag_certificates`     | [documentation](documentation/cki.cki_tools.refresh_dogtag_certificates.md)     |
| `cki.cki_tools.repo_manager`                    | [documentation](documentation/cki.cki_tools.repo_manager.md)                    |
| `cki.cki_tools.scratch_cleaner`                 | [documentation](documentation/cki.cki_tools.scratch_cleaner.md)                 |
| `cki.cki_tools.select_kpet_tree`                | [documentation](documentation/cki.cki_tools.select_kpet_tree.md)                |
| `cki.cki_tools.service_metrics`                 |                                                                                 |
| `cki.cki_tools.url_shortener`                   | [documentation](documentation/cki.cki_tools.url_shortener.md)                   |
| `cki.cki_tools.webhook_receiver`                | [documentation](documentation/cki.cki_tools.webhook_receiver.md)                |
| `cki.cki_tools.webhook_sentinel`                |                                                                                 |
| `cki.cki_tools.yaml`                            | [documentation](documentation/cki.cki_tools.yaml.md)                            |
| `cki.deployment_tools.autoscaler`               | [documentation](documentation/cki.deployment_tools.autoscaler.md)               |
| `cki.deployment_tools.deployment_bot`           | [documentation](documentation/cki.deployment_tools.deployment_bot.md)           |
| `cki.deployment_tools.gitlab_codeowners_config` | [documentation](documentation/cki.deployment_tools.gitlab_codeowner_config.md)  |
| `cki.deployment_tools.gitlab_repo_config`       |                                                                                 |
| `cki.deployment_tools.gitlab_runner_config`     | [documentation](documentation/cki.deployment_tools.gitlab_runner_config.md)     |
| `cki.deployment_tools.grafana`                  | [documentation](documentation/cki.deployment_tools.grafana.md)                  |
| `cki.deployment_tools.render`                   | [documentation](documentation/cki.deployment_tools.render.md)                   |
| `cki.kcidb.adjust_dumpfiles`                    |                                                                                 |
| `cki.kcidb.beaker_to_kcidb`                     |                                                                                 |
| `cki.kcidb.datawarehouse_submitter`             |                                                                                 |
| `cki.kcidb.datawarehouse_umb_submitter`         |                                                                                 |
| `cki.kcidb.edit`                                |                                                                                 |
| `cki.kcidb.forward_upstream`                    |                                                                                 |
| `cki.kcidb.get_test_summary`                    |                                                                                 |
| `cki.kcidb.parser`                              |                                                                                 |
| `cki.monitoring_tools.aws_cost_explorer`        | [documentation](documentation/cki.monitoring_tools.aws_cost_explorer.md)        |
| `cki.monitoring_tools.check_s3_bucket_size`     | [documentation](documentation/cki.monitoring_tools.check_s3_bucket_size.md)     |
| `cki.triager.main`                              | [documentation](documentation/cki.triager.main.md)                              |

## Detailed documentation of shell scripts

| Tool                                    | Description                                                          |
|-----------------------------------------|----------------------------------------------------------------------|
| `cki_deployment_acme.sh`                | [documentation](documentation/cki_deployment_acme.md)                |
| `cki_deployment_clean_docker_images.sh` | [documentation](documentation/cki_deployment_clean_docker_images.md) |
| `cki_deployment_codeowners_mr.sh`       |                                                                      |
| `cki_deployment_git_s3_sync.sh`         | [documentation](documentation/cki_deployment_git_s3_sync.md)         |
| `cki_deployment_grafana_backup.sh`      | [documentation](documentation/cki_deployment_grafana_backup.md)      |
| `cki_deployment_grafana_mr.sh`          | [documentation](documentation/cki_deployment_grafana_mr.md)          |
| `cki_deployment_osp_backup.sh`          | [documentation](documentation/cki_deployment_osp_backup.md)          |
| `cki_deployment_pgsql_backup.sh`        | [documentation](documentation/cki_deployment_pgsql_backup.md)        |
| `cki_deployment_pgsql_restore.sh`       | [documentation](documentation/cki_deployment_pgsql_restore.md)       |
| `cki_tools_git_cache_updater.sh`        | [documentation](documentation/cki_tools_git_cache_updater.md)        |
| `cki_tools_kernel_config_updater.sh`    | [documentation](documentation/cki_tools_kernel_config_updater.md)    |

<!-- vi: set spell spelllang=en: -->
