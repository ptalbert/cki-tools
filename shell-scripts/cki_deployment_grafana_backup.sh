#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - BUCKET_CONFIG_NAME: name of env variable with bucket configuration
# - GRAFANA_{URL,TOKEN}: Grafana credentials

# shellcheck disable=SC1091
. cki_utils.sh

if [ $# -lt 2 ]; then
    echo "Usage: ${0##*/} <group> <rotation>"
    exit 1
fi

# shellcheck disable=SC2154
cki_parse_bucket_spec "${BUCKET_CONFIG_NAME}"
# shellcheck disable=SC2154
AWS_S3=(aws s3 --endpoint "${AWS_ENDPOINT}")
# shellcheck disable=SC2154
S3_PATH="s3://${AWS_BUCKET}/${AWS_BUCKET_PATH}"
BACKUP_GROUP="$1"
BACKUP_ROTATION="$2"

BASE_DIR=$(mktemp -d)
trap 'rm -rf "${BASE_DIR}"' EXIT

DATA_PATH="${BASE_DIR}/dump"
mkdir -p "${DATA_PATH}"

cki_echo_yellow "Retrieving lists of previous backups..."
S3_LIST="${BASE_DIR}/s3-list.txt"
("${AWS_S3[@]}" ls "${S3_PATH}" || true) \
    | awk '{print $NF}' \
    | (grep "${BACKUP_GROUP}.tar.gz$" || true) \
    > "${S3_LIST}"
S3_COUNT="$(wc -l "${S3_LIST}" | awk '{print $1}')"

echo "  ${S3_COUNT} previous backups in group '${BACKUP_GROUP}' found"

cki_echo_yellow "Creating new backup..."
NAME="${S3_PATH}$(date +'%F-%H-%M').${BACKUP_GROUP}.tar.gz"
echo "  Dumping Grafana..."
python3 -m cki.deployment_tools.grafana download --path "${DATA_PATH}"
echo "  Compressing and uploading dump..."
tar --create --gzip --file - --directory "${DATA_PATH}" . | "${AWS_S3[@]}" cp - "${NAME}"
cki_echo_green "  successfully created at ${NAME}"

cki_echo_yellow "Removing previous backups..."
S3_LIST="${BASE_DIR}/s3-list.txt"
("${AWS_S3[@]}" ls "${S3_PATH}" || true) \
    | awk '{print $NF}' \
    | (grep "${BACKUP_GROUP}.tar.gz$" || true) \
    > "${S3_LIST}"
S3_COUNT="$(wc -l "${S3_LIST}" | awk '{print $1}')"

if [ "${S3_COUNT}" -gt "${BACKUP_ROTATION}" ]; then
    TO_REMOVE="$(head -n "-${BACKUP_ROTATION}" "${S3_LIST}")"
    # no quotes to get word splitting
    for i in ${TO_REMOVE}; do
        cki_echo_red "  removing ${i}..."
        "${AWS_S3[@]}" rm "${S3_PATH}${i}"
    done
else
    cki_echo_green "  no backups to remove"
fi
