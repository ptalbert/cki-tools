#!/usr/bin/python3
"""Parse pipeline or job and get KCIDB schemed data."""
import argparse
import json
import os
import pathlib

from cki_lib import gitlab
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
import datawarehouse
from gitlab.exceptions import GitlabGetError
import sentry_sdk

LOGGER = get_logger('cki.kcidb.datawarehouse_submitter')

GITLAB_URL = os.environ.get('GITLAB_URL')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN_SUBMITTER')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE')
RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')


def get_kcidb_data(job):
    """Return kcidb file content."""
    try:
        kcidb_data = json.loads(job.artifact('kcidb_all.json'))
    except GitlabGetError:
        LOGGER.info('Job %s has no kcidb_all.json file', job.name)
        return None

    return kcidb_data


def callback(args, body=None, **_):
    """Process one received job."""
    LOGGER.debug('Message Received: %s', body)
    LOGGER.info('Got message for %s - %i', body['project'], body['job_id'])

    with gitlab.get_instance(f'https://{body["gitlab_url"]}') as gitlab_instance:
        project = gitlab_instance.projects.get(body['project'])

    job = project.jobs.get(body['job_id'])

    kcidb_objects = get_kcidb_data(job)
    handle_output(args, kcidb_objects)


def process_queue(args):
    """Handle jobs for action=queue argument choice.

    Listen to RabbitMQ queue and process received jobs.
    """
    queue = MessageQueue(
        host=args.rabbitmq_host, port=args.rabbitmq_port,
        user=args.rabbitmq_user, password=args.rabbitmq_password,
        connection_params={
            'blocked_connection_timeout': 300, 'heartbeat': 600})

    queue.consume_messages(args.rabbitmq_exchange, ['#'],
                           lambda **kwargs: callback(args, **kwargs),
                           queue_name=args.rabbitmq_queue,
                           prefetch_count=5)


def process_single(args):
    """Process given job or pipeline id."""
    with gitlab.get_instance(f'https://{args.gitlab_url}') as gitlab_instance:
        project = gitlab_instance.projects.get(args.project)

    if args.kind == 'pipeline':
        pipeline = project.pipelines.get(args.id)
        jobs = pipeline.jobs.list(all=True)
    else:
        job = project.jobs.get(args.id)
        jobs = [job]

    for job in jobs:
        kcidb_objects = get_kcidb_data(job)
        handle_output(args, kcidb_objects)


def handle_output(args, data):
    """Handle generated data according to args."""
    if not data:
        return

    json_objects = json.dumps(data)

    if args.output_file:
        pathlib.Path(args.output_file).write_text(json_objects, encoding='utf8')

    if args.output_stdout:
        print(json_objects)

    if args.push:
        dw_api = datawarehouse.Datawarehouse(args.datawarehouse_url, args.datawarehouse_token)
        dw_api.kcidb.submit.create(data=data)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Parse a Pipeline or Job and get KCIDB schemed data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    group_output = parser.add_argument_group('Generated data destination.')
    group_output.add_argument(
        '--push', action='store_true', help='Push output data to Datawarehouse.',
        default=misc.is_production()
    )
    group_output.add_argument(
        '--datawarehouse-url', default=DATAWAREHOUSE_URL,
        help='URL to the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument(
        '--datawarehouse-token', default=DATAWAREHOUSE_TOKEN,
        help='Token for the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument('--output-file', help='Save output to a file.')
    group_output.add_argument('--output-stdout', action='store_true',
                              default=(not misc.is_production()),
                              help='Echo output to stdout.')

    subparser_action = parser.add_subparsers(dest='action', help='Action to perform.')

    parser_queue = subparser_action.add_parser(
        'queue', help='Monitor queue for new finished jobs.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_queue.add_argument('--rabbitmq-exchange', default=RABBITMQ_EXCHANGE,
                              help='Defaults to env RABBITMQ_EXCHANGE.')
    parser_queue.add_argument('--rabbitmq-queue', default=RABBITMQ_QUEUE,
                              help='Defaults to env RABBITMQ_QUEUE.')
    parser_queue.add_argument('--rabbitmq-host', default=RABBITMQ_HOST,
                              help='Defaults to env RABBITMQ_HOST.')
    parser_queue.add_argument('--rabbitmq-port', type=int, default=RABBITMQ_PORT,
                              help='Defaults to env RABBITMQ_PORT.')
    parser_queue.add_argument('--rabbitmq-user', default=RABBITMQ_USER,
                              help='Defaults to env RABBITMQ_USER.')
    parser_queue.add_argument('--rabbitmq-password', default=RABBITMQ_PASSWORD,
                              help='Defaults to env RABBITMQ_PASSWORD.')

    parser_single = subparser_action.add_parser(
        'single', help='Parse a single job or pipeline.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_single.add_argument('kind', help='pipeline or job?', choices=('pipeline', 'job'))
    parser_single.add_argument('gitlab_url', help='Gitlab host URL.')
    parser_single.add_argument('project', help='Gitlab project. Id or url-encoded path.')
    parser_single.add_argument('id', help='Gitlab pipeline or job id.')

    return parser.parse_args()


def main():
    """CLI Interface."""
    misc.sentry_init(sentry_sdk)
    args = parse_args()

    metrics.prometheus_init()

    if args.action == 'queue':
        process_queue(args)
    elif args.action == 'single':
        process_single(args)


if __name__ == '__main__':
    main()
