"""Triager checkers."""
from functools import lru_cache
from functools import partial

from cki_lib.logger import get_logger

from cki.triager import regexes
from cki.triager import session
from cki.triager.settings import FAIL_KICKSTART

LOGGER = get_logger('cki.triager.checkers')
REGEX_CHECKER = regexes.RegexChecker()


class FailureChecker:
    """FailureChecker Class."""

    check_functions = (
        'check_logs_with_regex',
    )

    def __init__(self, obj, misc=None):
        """Init."""
        self.obj = obj
        self.misc = misc or {}

    @property
    def logfiles(self):
        """
        Return a list of logfiles.

        Use output_files as a list of files to check.
        This method can be overriden on inherited classes to include different files.
        """
        log_files = []
        if hasattr(self.obj, 'log_url') and self.obj.log_url:
            log_files.append({'name': 'log', 'url': self.obj.log_url})

        if hasattr(self.obj, 'output_files') and self.obj.output_files:
            log_files.extend(self.obj.output_files)

        return log_files

    @classmethod
    def check(cls, obj, misc=None):
        """Perform checks and return result."""
        checker = cls(obj, misc)
        return checker.check_all()

    def check_all(self):
        """Run the checks."""
        failures = []
        for function in self.check_functions:
            LOGGER.info(' running: %s', function)
            result = getattr(self, function)()
            LOGGER.info('  result: %s', result)
            if result:
                failures.extend(result)

        LOGGER.info(' overall result: %s', failures)
        return failures

    @staticmethod
    @lru_cache(maxsize=1)
    def _download(file_url):
        """Return function to download a file."""
        content_type = (
            # Asume all .log files are plain text, their content-type is
            # octet-stream by default.
            'text/plain' if file_url.endswith('.log') else
            session.head(file_url).headers.get('content-type')
        )

        if content_type != 'text/plain':
            # Not a log file or no content-type
            return None

        log_content = session.get(file_url).content.decode(errors='ignore')
        return log_content

    def check_logs_with_regex(self):
        """Use regexes to find failures."""
        REGEX_CHECKER.download_lookups(self.misc.get('issueregex_ids'))
        failures = []
        for file in self.logfiles:
            failure = REGEX_CHECKER.search(partial(self._download, file['url']), file, self.obj)

            if failure:
                failures.extend(failure)

        return failures


class CheckoutFailureChecker(FailureChecker):
    """CheckoutFailureChecker Class."""

    @property
    def logfiles(self):
        """Return the checkout log files."""
        if getattr(self.obj, 'log_url', None):
            return [
                {'name': 'merge.log', 'url': self.obj.log_url}
            ]
        return []


class BuildFailureChecker(FailureChecker):
    """BuildFailureChecker Class."""

    @property
    def logfiles(self):
        """Return the build log files."""
        if getattr(self.obj, 'log_url', None):
            return [
                {'name': 'build.log', 'url': self.obj.log_url}
            ]
        return []


class TestFailureChecker(FailureChecker):
    """TestFailureChecker Class."""

    __test__ = False  # To supress a pytest warning

    check_functions = (
        FailureChecker.check_functions +
        (
            'check_kickstart_error',
        )
    )

    def check_kickstart_error(self):
        """
        Check job failed to Kickstart.

        If 'Boot test' has no duration, failed to provision.
        """
        if self.obj.comment == 'Boot test' and not self.obj.duration:
            return [FAIL_KICKSTART]

        return []
