"""Deployment bot utils."""
import re
from urllib import parse


def parse_project_url(project_url):
    """Parse a GitLab project URL.

    URL:
        https://gitlab.com/group/project

    Returns a tuple of (instance_url, path_with_namespace)
    """
    url_parts = parse.urlsplit(project_url)
    return (parse.urlunparse(url_parts[:2] + ('',) * 4), url_parts.path[1:])


def parse_job_url(job_url):
    """Parse a GitLab job URL.

    URL:
        https://gitlab.com/group/project/-/jobs/1234

    Returns a tuple of (instance_url, path_with_namespace, job_id)
    """
    url_parts = parse.urlsplit(job_url)
    return (parse.urlunparse(url_parts[:2] + ('',) * 4),
            re.sub('/-/.*', '', url_parts.path[1:]),
            int(re.sub('.*/-/jobs/', '', url_parts.path[1:])))


def parse_environment_url(environment_url):
    """Parse a GitLab environment URL.

    URL:
        https://gitlab.com/cki-project/cki-tools/-/environments/4158685

    Returns a tuple of (instance_url, path_with_namespace, environment_id)
    """
    url_parts = parse.urlsplit(environment_url)
    return (parse.urlunparse(url_parts[:2] + ('',) * 4),
            re.sub('/-/.*', '', url_parts.path[1:]),
            int(re.sub('.*/-/environments/', '', url_parts.path[1:])))
