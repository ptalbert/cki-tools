#! /usr/bin/python3
"""Listen to k8s events and print them to stdout."""
import datetime

from cki_lib import logger
from cki_lib.kubernetes import KubernetesHelper
import kubernetes as k8s
import prometheus_client

LOGGER = logger.get_logger(__name__)

METRIC_EVENTS_COUNT = prometheus_client.Counter(
    'cki_k8s_event_received',
    'Number of k8s events received',
    ['type']
)

# Initialize labels otherwise they're only published
# once the first message arrives.
METRIC_EVENTS_COUNT.labels('Warning')
METRIC_EVENTS_COUNT.labels('Normal')

METRIC_EVENTS_OOM_COUNT = prometheus_client.Counter(
    'cki_k8s_oom_event_generated',
    'Number of k8s oom events generated')


class EventListener():
    """Listen for events and log/evaluate them."""

    def __init__(self):
        """Run events listener."""
        self.k8s_helper = KubernetesHelper()
        self.k8s_helper.setup()
        self.start_timestamp = None

    def process_pod_oom(self, event):
        """Create an event if a pod OOM is detected."""
        if event.reason != 'Started' or event.involved_object.kind != 'Pod':
            LOGGER.debug("Not a pod starting, not checking for OOM.")
            return
        pod = self.k8s_helper.api_corev1().read_namespaced_pod(event.involved_object.name,
                                                               event.involved_object.namespace)
        for status in pod.status.container_statuses:
            if (not status.last_state.terminated or
                    status.last_state.terminated.reason != 'OOMKilled'):
                LOGGER.debug("Not an OOM-killed pod.")
                return

            self.k8s_helper.api_corev1().create_namespaced_event(
                event.involved_object.namespace,
                k8s.client.CoreV1Event(
                    metadata=k8s.client.V1ObjectMeta(generate_name=event.involved_object.name),
                    involved_object=event.involved_object,
                    type='Warning',
                    reason='PreviousContainerWasOOMKilled',
                    message=f'The previous instance of the container "{status.name}" was OOMKilled',
                ))
            METRIC_EVENTS_OOM_COUNT.inc()

    @staticmethod
    def log_event(event):
        """Log an event."""
        event_timestamp = event.last_timestamp or event.metadata.creation_timestamp
        print(
            # Remove tz info to match promtail timestamp parser
            event_timestamp.replace(tzinfo=None).isoformat() +
            f' - [{event.type}] - {event.involved_object.kind} - '
            f'{event.involved_object.name} - {event.message} - ({event.reason})'
        )

    def consume_stream(self, stream):
        """Consume messages from watcher.stream."""
        for raw_event in stream:
            event = raw_event['object']

            event_timestamp = event.last_timestamp or event.metadata.creation_timestamp
            if event_timestamp < self.start_timestamp:
                # At the beginning, list_namespaced_event returns a list of
                # old events. We only care for the ones that happen from start_timestamp
                # to avoid sending duplicated lines.
                LOGGER.debug("Old event, not logging.")
                continue

            METRIC_EVENTS_COUNT.labels(event.type).inc()

            self.log_event(event)
            self.process_pod_oom(event)

    def run(self):
        """Run events listener."""
        watcher = k8s.watch.Watch()
        stream = watcher.stream(
            self.k8s_helper.api_corev1().list_namespaced_event,
            namespace=self.k8s_helper.namespace,
            timeout_seconds=0
        )
        self.start_timestamp = datetime.datetime.now(datetime.timezone.utc)
        self.consume_stream(stream)
