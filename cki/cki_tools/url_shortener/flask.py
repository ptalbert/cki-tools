"""Shorten URLs (flask version)."""
from cki_lib import misc
import flask
import prometheus_client
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from . import shortener

app = flask.Flask(__name__)
misc.sentry_init(sentry_sdk, integrations=[FlaskIntegration()])

METRIC_SHORTURL_FORWARDED = prometheus_client.Counter(
    'cki_shorturl_forwarded', 'Number of short URLs forwarded')
METRIC_SHORTURL_CREATED = prometheus_client.Counter(
    'cki_shorturl_created', 'Number of short URLs created')


def _flask_handler(method, **kwargs):
    status_code, message = method(body=flask.request.get_data(),
                                  headers=flask.request.headers,
                                  **kwargs)
    if status_code in [301, 302, 303, 305, 307]:
        return flask.redirect(code=status_code, location=message)
    if status_code != 200:
        flask.abort(status_code, message)
    return message


@app.route('/', methods=['POST'])
def create_short_url():
    """Process a post request to create a new short URL."""
    METRIC_SHORTURL_CREATED.inc()
    return _flask_handler(shortener.create_short_url)


@app.route('/<short_digest>', methods=['GET'])
def forward_short_url(short_digest):
    """Process a get request to forward to the underlying long URL."""
    METRIC_SHORTURL_FORWARDED.inc()
    return _flask_handler(shortener.forward_short_url, short_digest=short_digest)
