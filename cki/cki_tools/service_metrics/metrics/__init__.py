"""Actual metrics."""

from .aws import AwsMetricsDaily
from .aws import AwsMetricsMinutely
from .beaker import BeakerMetrics
from .gitlab import GitLabMetricsHourly
from .gitlab import GitLabMetricsMinutely
from .kubernetes import KubernetesMetrics
from .s3buckets import S3BucketMetrics
from .sentry import SentryMetrics
from .teiidbeaker import TEIIDBeakerMetrics
from .volume import VolumeMetrics

ALL_METRICS = [
    AwsMetricsDaily,
    AwsMetricsMinutely,
    BeakerMetrics,
    GitLabMetricsHourly,
    GitLabMetricsMinutely,
    KubernetesMetrics,
    S3BucketMetrics,
    SentryMetrics,
    TEIIDBeakerMetrics,
    VolumeMetrics,
]
