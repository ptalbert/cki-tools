"""AWS cost metrics."""
import os

import boto3
from cki_lib import misc
from cki_lib.cronjob import CronJob
from cki_lib.logger import get_logger
import prometheus_client
import yaml

from cki.monitoring_tools import aws_cost_explorer

LOGGER = get_logger(__name__)
AWS_CONFIG = yaml.safe_load(os.environ.get('AWS_CONFIG', '{}'))


class AwsMetricsDaily(CronJob):
    """Calculate AWS metrics."""

    # once a day slightly after UTC midnight which should return the data of the previous UTC day
    schedule = '10 0 * * *'

    metric_cost = prometheus_client.Gauge(
        'cki_aws_cost',
        'grouped AWS costs',
        ['group', 'type', 'key']
    )
    metric_reserved_instance_end = prometheus_client.Gauge(
        'cki_aws_reserved_instance_end',
        'end date for reserved instances',
        ['reservedinstancesid', 'instancecount', 'instancetype', 'state']
    )

    def run(self, **_):
        """Update the metrics."""
        self.update_metric_cost()
        self.update_metric_reserved_instance_expiry()

    def update_metric_cost(self) -> None:
        """Update metric_cost."""
        for name, filters in misc.get_nested_key(AWS_CONFIG, 'cost/groups', {'all': {}}).items():
            explorer = aws_cost_explorer.AwsCostExplorer(days=1, filters=filters)
            for cost_type, cost_group in aws_cost_explorer.Cost.__members__.items():
                if cost_type == 'DAILY':
                    continue
                for cost in explorer.costs(cost_group):
                    self.metric_cost.labels(name, cost_type.lower(), cost[0]).set(cost[1])

    def update_metric_reserved_instance_expiry(self) -> None:
        """Update metric_reserved_instance_expiry."""
        ec2_client = boto3.Session().client('ec2')
        for instances in ec2_client.describe_reserved_instances()['ReservedInstances']:
            self.metric_reserved_instance_end.labels(
                instances['ReservedInstancesId'],
                instances['InstanceCount'],
                instances['InstanceType'],
                instances['State'],
            ).set(instances['End'].timestamp())


class AwsMetricsMinutely(CronJob):
    """Calculate AWS metrics."""

    schedule = '*/5 * * * *'  # every 5 minutes

    metric_subnets = prometheus_client.Gauge(
        'cki_aws_subnet_available_ip_address_count',
        'available IP addresses',
        ['vpcid', 'subnetid', 'availabilityzone', 'cidrblock', 'name']
    )

    def run(self, **_) -> None:
        """Update the metrics."""
        self.update_metric_subnets()

    def update_metric_subnets(self) -> None:
        """Update metric_subnets."""
        ec2_client = boto3.Session().client('ec2')
        for subnets in ec2_client.describe_subnets()['Subnets']:
            self.metric_subnets.labels(
                subnets['VpcId'],
                subnets['SubnetId'],
                subnets['AvailabilityZone'],
                subnets['CidrBlock'],
                next((t['Value'] for t in subnets.get('Tags', []) if t['Key'] == 'Name'), ''),
            ).set(subnets['AvailableIpAddressCount'])
