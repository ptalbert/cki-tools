# `shell-scripts/cki_deployment_clean_docker_images.sh`

Remove old docker images on the runners. A list of images to keep is built, and
all other images are removed.

The following images are kept:
- all :latest images
- all :production images
- the last three :mr-123 images
- the last three :p-123 images
- the last three :20210101.1 images
- all gitlab-runner-helper images

```shell
cki_deployment_clean_docker_images.sh
```

## Environment variables

| Field                        | Type   | Required | Description                                    |
|------------------------------|--------|----------|------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | string | no       | Images are only removed if set to `production` |
