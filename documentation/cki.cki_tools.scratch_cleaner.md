# `cki.cki_tools.scratch_cleaner`

Check whether the GitLab jobs for directories on the scratch volume are still
running. Scratch directories of already finished jobs are deleted.

```shell
python3 -m cki.cki_tools.scratch_cleaner [--scratch-volume PATH]
```

## Environment variables

| Field               | Type   | Required | Description                                                           |
|---------------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`     | dict   | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`      | string | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                |
| `IRCBOT_URL`        | no     | no       | IRC bot endpoint                                                      |
| `URL_SHORTENER_URL` | no     | no       | URL shortener endpoint to link to GitLab jobs in IRC messages         |
