# `cki.monitoring_tools.check_s3_bucket_size`

Summarize size and number of files on an S3 bucket.

```shell
Usage: python3 -m cki.monitoring_tools.check_s3_bucket_size
    [--quota SIZE]
    [--threshold SIZE]
    [--ignore-prefix]
    [BUCKET_SPEC]
```

The Bucket specification needs to be given as the name of an environment variable like

```
BUCKET_SOFTWARE="http://localhost:9000|cki_temporary|super_secret|software|subpath/"
```

The exit code will be non-zero if the total size of all objects in the bucket
(in GB) exceeds the given threshold.

When a quota (in GB) is specified, the output includes the relative usage.
