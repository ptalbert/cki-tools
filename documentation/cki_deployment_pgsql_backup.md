# `shell-scripts/cki_deployment_pgsql_backup.sh`

Backup a PostgreSQL database to an S3 bucket.

```shell
cki_deployment_pgsql_backup.sh <group> <count>
```

The last `<count>` backups per group `<group>` are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_pgsql_backup.sh daily    7
@weekly  cki_deployment_pgsql_backup.sh weekly   4
@monthly cki_deployment_pgsql_backup.sh monthly 12
@yearly  cki_deployment_pgsql_backup.sh yearly   2
```

## Environment variables

| Field                 | Type   | Required | Description                                               |
|-----------------------|--------|----------|-----------------------------------------------------------|
| `BUCKET_CONFIG_NAME`  | string | yes      | Name of an environment variable with bucket configuration |
| `POSTGRESQL_USER`     | no     | yes      | PostgreSQL user name                                      |
| `POSTGRESQL_PASSWORD` | yes    | yes      | PostgreSQL password                                       |
| `POSTGRESQL_HOST`     | no     | yes      | PostgreSQL host                                           |
| `POSTGRESQL_DATABASE` | no     | yes      | PostgreSQL database                                       |
