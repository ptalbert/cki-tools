# `cki.monitoring_tools.aws_cost_explorer`

Get a short summary of AWS costs.

```shell
Usage: python3 -m cki.monitoring_tools.aws_cost_explorer
    [--threshold COST]
    [--max-items COUNT]
    [--item-cutoff COST]
    [--filter FILTER...]
    [GROUP]
```

The following groups are available:

| Group           | Description                                         |
|-----------------|-----------------------------------------------------|
| `daily`         | Daily costs for the last week, most recent first    |
| `component`     | normalized 7-day average per `ServiceComponent` tag |
| `owner`         | normalized 7-day average per `ServiceOwner` tag     |
| `service`       | normalized 7-day average per AWS Service            |
| `usage_type`    | normalized 7-day average per AWS Usage Type         |
| `instance_type` | normalized 7-day average per AWS Instance Type      |
| `operation`     | normalized 7-day average per AWS API Operation      |

The exit code will be non-zero if the threshold set with `--threshold` is
exceeded for the previous day.

Filters specified with `--filter` can be used to limit the results to certain
tags, e.g.

```shell
python3 -m cki.monitoring_tools.aws_cost_explorer \
    --filter ServiceOwner=CkiProject \
    component
```

The number of shown items can be further limited by cost (`--item-cutoff`) or
count (`--max-items`).
